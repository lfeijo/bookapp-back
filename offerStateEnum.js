var OfferStateEnum = {
	AVAILABLE: 0,
	BORROWED: 1,
	DONATED: 2
};

module.exports = OfferStateEnum;