var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var app = express();

var secret = "bunda";

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var connection = require('./connect');

var User = require('./model/user');
var Campus = require('./model/uni');

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    
    res.header('Access-Control-Allow-Origin', req.get('origin'));
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization');

    next();
}

app.use(allowCrossDomain);

function isAuth(){
	return function(req, res, next) {
		var parts;
		if (req.headers.authorization) {
			parts = req.headers.authorization.split(' ');
			if (parts.length === 2) {
				if (parts[0]==="Bearer") {
					try {
						var info = jwt.verify(parts[1], secret);
						User.findOne({
								_id: info._id
							},
							function(err, user) {
								if (!err) {
									if (!(user===null||typeof user==='undefined')) {
										req.user = user;
										next();
									}
								}
							}
						);
					} catch(err) {
						res.status(403).json(err);
					}
				} else {
					res.status(403).send();
					// other auth method being used
				}
			} else {
				res.status(403).send();
				// header authorization is empty
			}
		} else {
			res.status(403).send();
			// header param undefined or null idk whatever
		}
	}
}

app.post('/auth', function (req, res) {
	if (typeof req.body.username !== 'undefined' && req.body.username !== null &&
		typeof req.body.password !== 'undefined' && req.body.password !== null) {
		User.findOne({"username": { $regex: new RegExp("^" + req.body.username + "$", "i") } },
			'+password',
			function(err, user){
				if (!err) {
					if (user !== null) {
						if (bcrypt.compareSync(req.body.password, user.password)) {
							res.status(200).jsonp(jwt.sign({_id: user._id}, secret));
						} else {
							res.status(403).jsonp({error: "Invalid credentials"});
						}
					} else {
						res.status(403).jsonp({error: "Invalid credentials"});
					}
				} else {
					res.status(409).jsonp({error: "Invalid credentials"});
				}
			}
		);
	} else {
		res.status(400).jsonp({error: "Fields left blank"});
	}
});

app.get('/auth', isAuth(), function(req, res){
	res.status(200).jsonp("true");
});


var userRoutes = require('./route/user')(isAuth);
app.use('/user', userRoutes);
var uniRoutes = require('./route/uni')(isAuth);
app.use('/uni', uniRoutes);
var offerRoutes = require('./route/offer')(isAuth);
app.use('/offer', offerRoutes);
var transactionRoutes = require('./route/transaction')(isAuth);
app.use('/transaction', transactionRoutes);


app.listen(8000);
