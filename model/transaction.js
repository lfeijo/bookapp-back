var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var OfferTypeEnum = require('../offerTypeEnum');
var OfferStateEnum = require('../offerStateEnum');
var User = require('../model/user');

// SCHEMA
var transactionSchema = new Schema({
	offer: { type: Schema.Types.ObjectId, ref: 'Offer' },
	when: { type: Date, default: Date.now() },
	ended: { type: Date },
	type: { type: Number, enum: [OfferTypeEnum.BORROW, OfferTypeEnum.DONATE] },
	from: { type: Schema.Types.ObjectId, ref: 'User' },
	to: { type: Schema.Types.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('Transaction', transactionSchema, 'transactions');