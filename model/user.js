var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Uni = require('./uni');


// SCHEMA
var userSchema = new Schema({
    name: { type: String, required: true },
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true, select: false },
    uni: { type: Schema.Types.ObjectId, ref: 'Uni', required: true },
    phone: { type: String, required: true },
    karma: { type: Number, default: 0 },
    transaction_c: { type: Number, default: 0 }
});

userSchema.path('name').validate(function(v){
	return /^[a-zA-z]{2,20} [a-zA-z]{1,20}$/.test(v);
}, "Invalid Name");

module.exports = (mongoose.model('User', userSchema, 'users'));