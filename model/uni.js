var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var OfferTypeEnum = require('../offerTypeEnum');
var OfferStateEnum = require('../offerStateEnum');
var User = require('../model/user');

// SCHEMA
var uniSchema = new Schema({
    name: { type: String, required: true, unique: true },
    city: { type: String, required: true }
});

uniSchema.index({'name':'text'});


module.exports = mongoose.model('Uni', uniSchema, 'unis');