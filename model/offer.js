var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var OfferTypeEnum = require('../offerTypeEnum');
var OfferStateEnum = require('../offerStateEnum');
var User = require('../model/user');
var Uni = require('../model/uni');

var searchPlugin = require('mongoose-text-search');

// SCHEMA
var offerSchema = new Schema({
	uni: { type: Schema.Types.ObjectId, ref: 'Uni' },
	isbn: { type: String, set: function(v){
        return (v.replace(/\D/g,'')).trim();
    } },
    title: String,
    author: String,
	by: { type: Schema.Types.ObjectId, ref: 'User' },
	thumb: { type: String },
	tags: [{ name: String }],
    state: { type: Number, enum: [OfferStateEnum.BORROWED,
                                  OfferStateEnum.AVAILABLE,
                                  OfferStateEnum.DONATED], default: OfferStateEnum.AVAILABLE },
    when: { type: Date, default: Date.now() }
});


offerSchema.plugin(searchPlugin);
offerSchema.index({
	title: "text",
	author: "text",
	isbn: "text"
},{
	name: "offer_index",
	weights: {
		title: 2,
		author: 2,
		isbn: 1
	}
});


module.exports = mongoose.model('Offer', offerSchema, 'offers');

// .path('by').validate(function(value, respond){
// 	User.findOne(
// 		{_id: value},
// 		function(err, doc){
// 			if(err || !doc) respond(false);
// 			else respond(true); 
// 		}
// 	);
// }, 'Invalid user id')

// .path('uni').validate(function(value, respond){
// 	Uni.findOne(
// 		{_id: value},
// 		function(err, doc){
// 			if(err || !doc) respond(false);
// 			else respond(true);
// 		}
// 	);
// }, 'Invalid uni id');
