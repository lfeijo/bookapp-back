module.exports = function(isAuth){
    var express = require('express');
    var router = express.Router();
    var Uni = require('../model/uni');

    // get all unis
    // no params
    router.get('/', function(req, res){

        this.respond = function(docs){
            res.status(200).jsonp(docs);
        };

        this.error = function(err){
            res.status(400).jsonp({error:err});
        };

        Uni.find({},'name city').exec()
        .then(this.respond, this.error);

    });

    // get uni info by id
    // URI params: uniid
    router.get('/:uniid', function(req, res){

        this.respond = function(docs){
            res.status(200).jsonp(docs);
        };

        this.error = function(err){
            res.status(400).jsonp({error:err});
        };

        Uni.findOne({ _id: req.params.uniid },'name city').exec()
        .then(this.respond, this.error);

    });

    // create new uni
    // POST params: name, city
    router.post('/', function(req, res){

        var uni = new Uni();
        uni.name = req.body.name;
        uni.city = req.body.city;

        this.respond = function(uni) {
            res.status(201).jsonp(uni);
        };
        this.error = function(err) {
            res.status(400).jsonp({error:err});
        };

        uni.save()
        .then(this.respond, this.error);
        
    });

	return router;
};