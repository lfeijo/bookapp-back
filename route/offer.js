module.exports = function(isAuth){
    var express = require('express');
    var router = express.Router();

    var User = require('../model/user');
    var Uni = require('../model/uni');
    var Offer = require('../model/offer');
    var Transaction = require('../model/transaction');

    var OfferTypeEnum = require('../offerTypeEnum');
    var OfferStateEnum = require('../offerStateEnum'); 

    var respond = function(docs){
        this.status(200).jsonp(docs);
    };

    var error = function(err){
        this.status(400).jsonp({error:err});
    };

    // gets the available/borrowed offers for the given uni id
    // URI params: uniid
    router.get('/uni/:uniid', isAuth(), function(req, res){

        if (typeof req.params.uniid === 'undefined' || req.params.uniid === null){
            error("invalid URI params");
            return;
        }

    	Offer
        .find({uni:req.params.uniid, state: {$ne: OfferStateEnum.DONATED}})
        .sort({when:-1})
        .populate('by uni')
        .exec()
        .then(respond.bind(res), error.bind(res));

    });

    // gets all available/borrowed offers for user's uni
    // no params
    router.get('/', isAuth(), function(req, res){

        Offer
        .find({uni: req.user.uni, state: {$ne: OfferStateEnum.DONATED}})
        .sort({when:-1})
        .populate('by uni')
        .exec()
        .then(respond.bind(res), error.bind(res));

    });

    // search by all offer's fields at user's uni only
    // GET params: query
    router.get('/search', isAuth(), function(req, res){

        if (typeof req.query.query === 'undefined' || req.query.query === null){
            error("Must provide a nonempty query param");
            return;
        }

        Offer.find({
            $or: [{title: {$regex: new RegExp(req.query.query,'i')}},
                  {author: {$regex: new RegExp(req.query.query,'i')}}]
        })
        .populate('by uni')
        .exec()
        .then(respond.bind(res), error.bind(res));

    });

    // search for available/borrowed offers by isbn in user's uni
    // GET params: isbn
    router.get('/search/isbn', isAuth(), function(req, res){

        if (typeof req.query.isbn === 'undefined' || req.query.isbn === null) {
            error("Must provide a nonempty query for isbn");
            return;
        }

        Offer
        .find({
            'isbn': (((req.query.isbn).replace(/\D/g,'')).trim()),
            uni: req.user.uni,
            state: {$ne: OfferStateEnum.DONATED}})
        .sort({when:-1})
        .populate('by uni')
        .exec()
        .then(respond.bind(res), error.bind(res));

    });

    // gets the offer with given id
    // URI params: /offerid
    router.get('/:offerid', isAuth(), function(req, res){

        if (typeof req.params.offerid === 'undefined' || req.params.offerid === null){
            error("invalid URI params");
            return;
        }
        
        Offer
        .findOne({ '_id': req.params.offerid })
        .populate('by uni')
        .exec()
        .then(respond.bind(res), error.bind(res));
        
    });

    // adds an offer linked to the logged in user
    // POST params: isbn, title, author, tags (future)
    router.post('/', isAuth(), function(req, res){

        if (typeof req.body.isbn === 'undefined' || req.body.isbn === null ||
            typeof req.body.title === 'undefined' || req.body.title === null ||
            typeof req.body.author === 'undefined' || req.body.author === null) {
            error("Fill all POST params pls");
            return;
        }

        new Offer({
            uni: req.user.uni,
            isbn: req.body.isbn,
            title: req.body.title,
            thumb: req.body.thumb,
            author: req.body.author,
            by: req.user._id
        })
        .save()
        .then(respond.bind(res), error.bind(res));

    });

    // gets offers by given user
    // URI params: userid
    router.get('/by/:userid', isAuth(), function(req, res){

        if (typeof req.params.userid === 'undefined' || req.params.userid === null){
            error("invalid URI params");
            return;
        }

        Offer
        .find({ 'by': req.params.userid })
        .sort({ when: -1 })
        .populate('by uni')
        .exec()
        .then(respond.bind(res), error.bind(res));

    });


    // update offer based on given id
    // updatable fields are title, author, isbn.
    router.put("/:id", isAuth(), function(req, res){
        if (req.params.id !== null && typeof req.params.id !== 'undefined' &&
            ((req.body.title !== null && typeof req.body.title !== 'undefined') &&
            (req.body.author !== null && typeof req.body.author !== 'undefined') &&
            (req.body.isbn !== null && typeof req.body.isbn !== 'undefined'))){

            Offer
            .findOne({
                _id: req.params.id
            })
            .exec()
            .then(
                function(doc){
                    if (doc.by.toString() === req.user._id.toString()) {
                        Offer.update(
                            { _id: req.params.id },
                            { title: req.body.title,
                              author: req.body.author,
                              isbn: req.body.isbn },
                            function(err, doc){
                                if (!err) {
                                    res.status(200).jsonp(doc);
                                } else {
                                    res.status(400).jsonp({error:err});
                                }
                            });
                    } else {
                        res.status(409).jsonp({error:"Permission denied to modify this offer"});
                    }
                },
                function(err){
                    res.status(400).jsonp({error:err});
                }
            );

        } else {
            res.status(409).jsonp({error:"Fill all fields before submitting"});
        }
    });

	return router;
};