module.exports = function(isAuth){
    var express = require('express');
    var router = express.Router();
    
    var User = require('../model/user');
    var Uni = require('../model/uni');
    var Offer = require('../model/offer');
    var Transaction = require('../model/transaction');

    var OfferTypeEnum = require('../offerTypeEnum');
    var OfferStateEnum = require('../offerStateEnum'); 

    // if given offer is available
    // create transaction for given offer id with post parameters type and to
    // URI params: offerid
    // POST params: type (1 borrow or 2 donate), to
    router.post('/:offerid', isAuth(), function(req, res){
        if (typeof req.body.type === 'undefined' || req.body.type === null ||
            typeof req.body.to === 'undefined' || req.body.to === null || 
            req.body.type < '1' || req.body.type > '2' ) {
            res.status(400).jsonp({error:"Must provide transaction type and addressee via post"});
        } else {
            User.findOne(
                {_id:req.body.to},
                function(err, doc){
                    if (!err && doc !== null) { // 'to' user exists!
                        if (doc._id !== req.user._id) {
                            Offer.update( // set offer as not available and verify if it's in user's uni
                                {_id:req.params.offerid, state: OfferStateEnum.AVAILABLE, uni: req.user.uni, by: req.user._id},
                                {state: req.body.type},
                                function(err, numAffected){
                                    if (!err) {
                                        if (numAffected.ok===1) {
                                            var tran = new Transaction({
                                                offer: req.params.offerid,
                                                type: req.body.type,
                                                from: req.user._id,
                                                to: req.body.to
                                            });
                                            tran.save(function(err, doc){
                                                if (!err && doc !== null) {
                                                    if (doc.type=='2') { // DONATE
                                                        User.update(
                                                            {$or:[{_id:req.user._id},{_id:req.body.to}]}, // both users
                                                            {$inc:{karma:1, transaction_c:1}}, // get 1 karma point
                                                            function(err, doc){
                                                                if (!err) {
                                                                    res.status(201).jsonp(doc);
                                                                } else{
                                                                    res.status(400).jsonp({error:err});
                                                                }      
                                                            }
                                                        );   
                                                    } else { // not donation, no karma yet
                                                        res.status(201).jsonp(doc);
                                                    }
                                                } else { // error on saving transaction
                                                    res.status(400).jsonp({error:err});
                                                }
                                            });
                                        } else { // update was done on offer but numAffected is 0
                                            res.status(400).jsonp({error:"???"});
                                        }
                                    } else { // one of update's many conditions failed
                                        res.status(400).jsonp({error:err}); // offer not available for the user that requested it
                                    }
                                }
                            );
                        } else {
                            res.status(400).jsonp({error:"One can't create transaction with self"});
                        }
                    } else { // 'to' user doesnt exist
                        res.status(400).jsonp({error:"Addressee doesn't exist"});
                    }
                }
            );
        }
    });

    // finalize transaction
    // sets offer state back to 0
    // adds 1 karma point to the users involved
    // URI params: offerid
    // To do: add support for bad karma
    router.post('/:transactionid/finalize', isAuth(), function(req, res){
        if (typeof req.body.eval === 'undefined' || req.body.eval === null) {
            res.status(400).jsonp({error:"Must provide an evaluation bool value for the transaction"});
        } else {
            Transaction.update(
                { '_id': req.params.transactionid },
                { 'ended': Date.now() },
                function(err, numAffected){
                    if (!err) {
                        Transaction.findOne(
                            {_id:req.params.transactionid},
                            function(err, trans){
                                if (!err) {
                                    Offer.update(
                                        { _id: trans.offer },
                                        { state: OfferStateEnum.AVAILABLE },
                                        function(err, numAffected){
                                            if (!err) {
                                                User.update(
                                                    { _id: req.user._id },
                                                    { $inc: { karma: 1, transaction_c: 1 }},
                                                    function(err, numAffected){
                                                        if (!err) {
                                                            var val = eval>=1?1:-1; // if eval was positive: 1, else -1
                                                            User.update(
                                                                { _id: trans.to },
                                                                { $inc: { karma: val, transaction_c: 1 }},
                                                                function(err, numAffected){
                                                                    if (!err) {
                                                                        res.status(200).jsonp(numAffected);
                                                                    } else {
                                                                        res.status(400).jsonp({error:err});
                                                                    }
                                                                }
                                                            );
                                                        } else {
                                                            res.status(400).jsonp({error:err});
                                                        }
                                                    }
                                                );
                                            } else {
                                                res.status(400).jsonp({error:err});
                                            }
                                        }
                                    );
                                } else {
                                    res.status(400).jsonp({error:err});
                                }
                            }
                        );
                    } else {
                        res.status(400).jsonp({error:err});
                    }
                }
            );
        }
    });

    router.get('/unfinished', isAuth(), function(req, res){
        Transaction.find({ from: req.user._id, ended: {$exists: false} })
        .populate('offer from to')
        .exec(
            function(err, transactions){
                if (!err) {
                    res.status(200).jsonp(transactions);
                } else {
                    res.status(400).jsonp({error:err});
                }
            }
        );
    });

    router.get('/finished', isAuth(), function(req, res){
        Transaction.find({ from: req.user._id, ended: {$exists: true} })
        .populate('offer from to')
        .exec(
            function(err, transactions){
                if (!err) {
                    res.status(200).jsonp(transactions);
                } else {
                    res.status(400).jsonp({error:err});
                }
            }
        );
    });

    router.get('/unfinished/both', isAuth(), function(req, res){
        Transaction.find({ $or: [{from: req.user._id},{to: req.user._id}], ended: {$exists: false} })
        .populate('offer from to')
        .exec(
            function(err, transactions){
                if (!err) {
                    res.status(200).jsonp(transactions);
                } else {
                    res.status(400).jsonp({error:err});
                }
            }
        );
    });

    router.get('/finished/both', isAuth(), function(req, res){
        Transaction.find({ $or: [{from: req.user._id},{to: req.user._id}], ended: {$exists: true} })
        .populate('offer from to')
        .exec(
            function(err, transactions){
                if (!err) {
                    res.status(200).jsonp(transactions);
                } else {
                    res.status(400).jsonp({error:err});
                }
            }
        );
    });

    router.get('/both', isAuth(), function(req, res){
        Transaction.find({ $or: [{from: req.user._id},{to: req.user._id}]})
        .populate('offer from to')
        .exec(
            function(err, transactions){
                if (!err) {
                    res.status(200).jsonp(transactions);
                } else {
                    res.status(400).jsonp({error:err});
                }
            }
        );
    });

    router.get('/', isAuth(), function(req, res){
        Transaction.find({ from: req.user._id })
        .populate('offer from to')
        .exec(
            function(err, transactions){
                if (!err) {
                    res.status(200).jsonp(transactions);
                } else {
                    res.status(400).jsonp({error:err});
                }
            }
        );
    });

	return router;
};