var bcrypt = require('bcrypt');

module.exports = function(isAuth){

    var express = require('express');
    var router = express.Router();
    var salt = bcrypt.genSaltSync(10);

    var User = require('../model/user');
    var Uni = require('../model/uni');

    // get all users
    router.get("/", isAuth(), function(req, res){
        User.find({}, function(err, user){
            if(!err){
                res.status(200).jsonp(user);
            } else {
                res.status(501);
            }
        });
    });

    // get user by id
    router.get("/:id", isAuth(), function(req, res){
        if (typeof req.params.id !== 'undefined' && req.params.id !== null) {
            User.findOne({ _id: req.params.id }, function(err, user){
                if (!err) {
                    if (typeof user === 'undefined' || user == null) {
                        res.status(404).jsonp({error:"Invalid id"});
                    } else res.status(200).jsonp(user);
                } else {
                    res.status(404).jsonp({error:err});
                }
            });
        } else {
            res.status(404).jsonp({error:"Invalid parameters"});
        }
    });

    // get user by username
    router.get("/username/:username", isAuth(), function(req, res){
        if (typeof req.params.username !== 'undefined' && req.params.username !== null && req.params.username !== "") {
            User.findOne({ username: { $regex: new RegExp("^"+req.params.username+"$", 'i')} }, function(err, user){
                if (!err) {
                    if (typeof user === 'undefined' || user === null) {
                        res.status(404).jsonp({error:"Invalid username"});
                    } else res.status(200).jsonp(user);
                } else {
                    res.status(404).jsonp({error:err});
                }
            });
        } else {
            res.status(404).jsonp({error:"Invalid parameters"});
        }
    });

    // add new user
    router.post("/", function(req, res){
        var newUser = new User();
        if (req.body.name === null || typeof req.body.name === 'undefined' ||
            req.body.username === null || typeof req.body.username === 'undefined' ||
            req.body.password === null || typeof req.body.password === 'undefined' ||
            req.body.uni === null || typeof req.body.uni === 'undefined' ||
            req.body.phone === null || typeof req.body.phone === 'undefined'){
            res.status(409).jsonp({error:"Fill all fields before submitting"});
        } else {
            newUser.name = req.body.name.trim();
            newUser.username = req.body.username;
            newUser.password = bcrypt.hashSync(req.body.password,salt);
            newUser.uni = req.body.uni;
            newUser.phone = req.body.phone;
            newUser.save(function(err, doc){
                if (!err) {
                    res.status(201).jsonp(doc);
                } else {
                    res.status(409).jsonp({error:err});
                }
            });
        }
    });

    // update user based on given id
    // updatable fields are name, phone.
    router.put("/", isAuth(), function(req, res){
        if (req.body._id !== null && typeof req.body._id !== 'undefined' &&
            ((req.body.name !== null && typeof req.body.name !== 'undefined') &&
            (req.body.phone !== null && typeof req.body.phone !== 'undefined'))){

            if (req.user._id.toString() === req.body._id) {

                User.update(
                    { _id: req.body._id },
                    { name: req.body.name,
                      phone: req.body.phone },
                    function(err, doc){
                        if (!err) {
                            res.status(200).jsonp(doc);
                        } else {
                            res.status(400).jsonp({error:err});
                        }
                    });

            } else {
                res.status(409).jsonp({error:"Permission denied to modify this user"});
            }

        } else {
            res.status(409).jsonp({error:"Fill all fields before submitting"});
        }
    });

    return router;

};