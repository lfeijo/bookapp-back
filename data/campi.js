[{
	name: "Pontificia Universidade Católica do Rio Grande do Sul",
	campus: [
		"Central",
		"Viamão"
	],
	city: "Porto Alegre, RS"
},
{
	name: "Universidade Federal do Rio Grande do Sul",
	campus: [
		"Centro",
		"Saúde",
		"Vale",
		"Esportivo"
	],
	city: "Porto Alegre, RS"
}]