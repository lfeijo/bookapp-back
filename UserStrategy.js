var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

module.exports = function(User){

	return function() {
	    User.findOne({ username: _username }, function(err, user){
	        if(err){ console.log(err); return done(err); }
	        if(!user) { return done(null, false); }
	        if(!bcrypt.compareSync(password, user.password)){ return done(null, false); }
	        return done(null, user);
	    });
	}

};