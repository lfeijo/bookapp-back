var OfferTypeEnum = {
	BORROW: 1,
	DONATE: 2
};

module.exports = OfferTypeEnum;